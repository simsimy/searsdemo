﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearsCodeNamer.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SearsCodeNamer.Models;
namespace SearsCodeNamer.Controllers.Tests
{
    [TestClass()]
    public class ClassNamerControllerTests
    {
        [TestMethod()]
        public void Index()
        {
           
            var result = controller.Index() as JsonResult;
            JavaScriptSerializer ser = new JavaScriptSerializer();

            Assert.IsNotNull(result.Data);
        }
        [TestMethod()]
        public void Many()
        {
           
            var result = controller.Many() as JsonResult;

            dynamic data = result.Data;

            Assert.AreEqual(3, data.Count);
            
        }
        ClassNamerController controller = new ClassNamerController();
        [TestMethod()]
        public void Cloud()
        {
             
            JsonResult result = controller.Cloud() as JsonResult;

            TagCloudViewModel data = result.Data as TagCloudViewModel;
         
            int minAccurnces = data.MinAccurnces;
            int maxAccurnces = data.MaxAccurnces;
            
            Assert.AreEqual(200, data.Classes.Count);
            foreach (var tag in data.Tags)
            {
                Assert.AreEqual(tag.Value, data.Classes.Count(s => s.Contains(" " + tag.Key + " ")));
            }
            Assert.AreEqual(data.MinAccurnces, data.Tags.OrderBy(s => s.Value).First().Value);
            Assert.AreEqual(data.MaxAccurnces, data.Tags.OrderByDescending(s => s.Value).First().Value);
               

        }
    }
}
