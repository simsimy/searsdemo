﻿if (!String.prototype.contains) {
    String.prototype.contains = function (s, i) {
        return this.indexOf(s, i) != -1;
    }
}
$(document).ready(function () {

    page = getPage();

    ChangeAddress("loading");

    switch (page) {
        case "cloud":
            getCloud();
            break;
        case "many":
            getMany();
            break;
        default:

            getOne();
            break;



    }
});
function HideAll() {
    $('#feature_1').hide();
    $('#f1Classes').empty();
    $('#f2Classes').empty();
    $('#f3Tags').empty();
    $('#f3Classes').empty();
    $('#feature_2').hide();
    $('#feature_3').hide();
}
function getPage(hash) {
    if (typeof hash === 'undefined') hash = $(location).attr('hash').toLocaleLowerCase();

    if (hash == "#cloud") {

        return "cloud";
    }
    if (hash == "#many") {

        return "many";
    }
    if (hash == "#loading") {

        return "loading";
    }
    return "";
}
function getOne() {
    if ((getPage() == "")) return;
    $("#loading").show();
    $.getJSON("./ClassNamer/", function (data) {
        HideAll();
        $('#feature_1').show();
        $("#f1Classes").text(data);
        $("#loading").hide();
        ChangeAddress("")
    });
}
function getMany() {
    if (getPage() == "many") return;
    $("#loading").show();
    $.getJSON("./ClassNamer/Many", function (data) {
        HideAll();
        $('#feature_2').show();
        $("#f2Classes").text(data[0] + ", " + data[1] + " and " + data[2]);
        $("#loading").hide();
        ChangeAddress("Many");
    });
}
function getCloud() {
    if (getPage() == "cloud") return;
    $("#loading").show();
    $.getJSON("./ClassNamer/Cloud", function (data) {
        HideAll();
        $('#feature_3').show();
        var items = [];
        var min = data.MinAccurnces;
        var max = data.MaxAccurnces;
        var diff = max - min;
        if (diff == 0) diff = 1;

        $.each(data.Tags, function (key, val) {
            size = 15 + ((val - min) / diff) * 20;
            items.push("<span id='" + key + "' style='font-size: " + size + "px' >" + key + "</span>");
        });
        tagClasses = data.Classes;

        $("#f3Tags").html(shuffleArray(items).join(""));
        $("#f3Tags span").click(function (e) {

            DisplayClassesFor(e.target.id);
        });
        $("#loading").hide();
        ChangeAddress("Cloud");
    });
}
var tagClasses = null;
function DisplayClassesFor(tag) {
    var items = [];
    if (tagClasses == null) return;

    for (var i in tagClasses) {
        className = tagClasses[i];
        console.log(i);
        console.log(className);
        console.log((""+className).contains(" "));
        console.log(tag);
        
        if (className.contains(" " + tag + " "))
            items.push("<li>" + className.replace(/ /g, "") + "</li>");
    }
    $("#f3Classes").html(items.join(""));

}
/**
* Randomize array element order in-place.
* Using Fisher-Yates shuffle algorithm.
*/
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function ChangeAddress(address) {
    // var stateObj = { foo: "bar" };
    history.pushState({}, address, "#" + address);
}