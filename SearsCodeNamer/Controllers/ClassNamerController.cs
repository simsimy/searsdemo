﻿using SearsCodeNamer.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SearsCodeNamer.Controllers
{
    public class ClassNamerController : Controller
    {
         static string ConverCamelCase(string str)
        {
            return " " + Regex.Replace(str, "[a-z][A-Z]", m => m.Value[0] + " " + m.Value[1]) + " ";
        }
        //
        // GET: /ClassNamer/
         static string GetClassName(bool splitted = false)
        {
            string url = "http://www.classnamer.com/index.txt";
            WebClient client = new WebClient();

            string res = client.DownloadString(url).Trim();
            if (splitted) return ConverCamelCase(res);
            else return res;

        }
         static IEnumerable<string> GetManyStrings(int ammound, bool splitted = false)
        {
            ConcurrentBag<string> res = new ConcurrentBag<string>();
            Parallel.For(0, ammound, (i) => { res.Add(GetClassName(splitted)); });
            return res;
        }
        [HttpGet]
        public ActionResult Index()
        {

            return Json(GetClassName(false), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Many()
        {
            return Json(GetManyStrings(3, false).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Cloud()
        {
            List<string> names = GetManyStrings(200, true).ToList();
            Dictionary<string, int> tags = names.SelectMany(s => s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).GroupBy(s => s).ToDictionary(s => s.Key, s => s.Count());
           
          var tgs =tags.OrderByDescending(s => s.Value);
  
            return  new JsonResult()
            {
                Data = new TagCloudViewModel
                {
                    Tags = tgs.ToDictionary(x => x.Key, y => y.Value),
                    Classes = names,
                    MinAccurnces = tgs.Last().Value,
                    MaxAccurnces = tgs.First().Value,
                },
                  JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}
