﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SearsCodeNamer.Models
{
    [JsonObject]
    [Serializable]
    public class TagCloudViewModel
    {
        [JsonProperty]
        public Dictionary<string, int> Tags { get; set; }
        [JsonProperty]
        public List<string> Classes { get; set; }
        [JsonProperty]
        public int MinAccurnces { get; set; }
        [JsonProperty]
        public int MaxAccurnces { get; set; }
    }
}